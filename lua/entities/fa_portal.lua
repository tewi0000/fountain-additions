-- TODO: use cvm_math
function math.clamp(n, low, high) return math.min(math.max(n, low), high) end

local COLOR = nil
if CLIENT then
	COLOR = Color(95, 40, 190)
end

function ENT:SetupDataTable()
	self:AddDataTableEntry("SizeX",   "Int", 100)
	self:AddDataTableEntry("SizeY",   "Int", 100)
	self:AddDataTableEntry("TargetX", "Int", 100)
	self:AddDataTableEntry("TargetY", "Int", 100)
end

function ENT:Tick()
	if CLIENT then return end
	local pos = self:GetPos()
	local rect = rectangle.Rectangle(pos, Vector(self:GetSizeX(), self:GetSizeY()))
	for id, ent in ipairs(ents.GetByType("qar5_character")) do
		if rect:Intersects(ent:GetColliderWhole()) then
			local offset = pos.x - ent:GetPos().x
			ent:SetPos(Vector(self:GetTargetX() - offset, self:GetTargetY()))
		end
	end
end

function ENT:Draw()
	local pos = self:GetPos()
	draw.SetColor(COLOR)
	draw.Rect(pos.x, pos.y, self:GetSizeX(), self:GetSizeY())
end
